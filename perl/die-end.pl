#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

print "Hello!\n";

die "oops";

print "Goodbye\n";

exit;

END {
    print "finally\n";
}

__END__

スクリプトで die しても exit しても、最後に END ブロックは呼ばれます。

呼ばれないのは panic したときくらい？
